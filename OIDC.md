# OIDC setup

  * certifikáty:
    * AS:
      * server ssl certifikat pro VS autorizacniho serveru
      * jwt_sign (RSA 4k)
    * Client
      * server ssl certifikat pro VS clienta
      * chain pro certifikat autorizacniho serveru
  * DNS:
    * musi existovat záznamy pro autorizační server i client virtual servery
## 1 Autorizacni server
### 1.1 JWT - vytvořit konfiguraci
  * Access  ››  Federation : JSON Web Token : Key Configuration  ››  New...
    * vypnit ID, jinak nejde zvolit v oauth profile
    * type: RSA
    * Signing Alg.: RS512
    * file & key jwt_sign
  * Access  ››  Federation : JSON Web Token : Token Configuration  ››  New...
    * Keys (JWT): 
      * issuer: hostname pro issuer, stejny jako pro autorizacni server. https://as.lab 
      * Signing Algorithms: vse krome none
      * Allowed pridat vytvoreny
### 1.2 Scope/Claim:
Scopes are identifiers used to specify what access privileges are being requested. Claims are name/value pairs that contain information about a user.\
Example of a good scope would be "read_only". Whilst an example of a claim would be "email": "john.smith@example.com".\
You can send claims in the id token (or JWT), or/and have them available via the userinfo endpoint (if using the "openid" scope).\

  * vytvořit scope - např. email, jmeno. OpenID scope existuje, není potřeba vytvářet.
  * vytvořit claim jmeno:
    * type: string
    * name: jmeno
    * value: %{session.last.logon.username}
### 1.3 Client application
  * name, application name: \[nazev aplikace. u me client.lab\]
  * grant type: Authorization Code / Hybrid
  * redirect URL(s) - povolené redirect URL kde aplikace přijímá authorization code - https://[url client vs]]/oauth/client/redirect
  * Support OpenID Connect: Enabled
  * Authorization Type: Secret
  * Scopes - něco vyberte
  * po potvrzeni otevrete vytvorenou definici clienta a ulozte si ID a secret

### 1.4 OAuth Profile:
  * client application - vybrat vytvořenou
  * Support JWT Token: enabled
  * Support OpenID Connect: enabled
  * Issuer: https://\[hostname autorizacniho serveru, stejne jako v 1.1\]
  * JWT Primary Key: klic z 1.1
  * ID Token Primary Key: klic z 1.1
  * ID token claims - vybrat vytvorene
  * UserInfo Claims - vybrat vytvorene
  * JWT Refresh Token Encryption Secret: vytvorit
### 1.5 Access profile:
  * pripravit autentizacni metodu, u me local DB
  * Access  ››  Profiles / Policies : Access Profiles (Per-Session Policies)  ››  New Profile...
    * Profile type: all
    * Profile Scope: Profile
    * OAuth Profile: vytvoreny
    * Languages: vybrat
  * Access policy:\
  ![access policy](OIDC-images/as-accesspolicy.png)
  * OAuth Authorization agent:
    * Scope/Claim Assign: vyberte vytverene scope, claim.
  * Přiřadit Access Policy K VS
## 2 Client
### 2.1 OAuth Provider
  * Access  ››  Federation : OAuth Client / Resource Server : Provider  ››  New...
  * Type: F5
  * Trusted Certificate Authorities - podle certifikatu, pokud je self signed tak import, vybrat
  * Allow Self-Signed JWK Config Certificate	
  * Use Auto JWT
  * OpenID URI: https://\[hostname autorizacniho serveru\]/f5-oauth2/v1/.well-known/openid-configuration
  * Kliknout na discover, melo by projit.
### 2.2 DNS Resolver
  * Network  ››  DNS Resolvers : DNS Resolver List, Create, potvrdit
  * otevrit vytvoreny resolver, ve forward zones nastavit zonu a NS tak, aby dokazal komunikaovat na autorizacni server
### 2.3 OAuth Server
  * Access  ››  Federation : OAuth Client / Resource Server : OAuth Server  ››  New OAuth Server Configuration...
  * name napr. as.lab-server   
  * mode: client
  * Type: F5
  * OAuth Provider - z kroku 2.1
  * DNS Resolver - z kroku 2.2
  * Client ID - z kroku 1.3
  * Client Secret - z kroku 1.3
  * Client ServerSSL Profile Name: serverssl-secure
### 2.4 Access Profile
  * Access  ››  Profiles / Policies : Access Profiles (Per-Session Policies)  ››  New Profile...
  * Profile Type: All
  * Profile Scope: Profile
  * Logout URI Include: /logout
  * Languages - pridat
  * Otevrit Visual Policy Editor\
  ![access policy](OIDC-images/cl-accesspolicy.png)
    * Do policy pridat OAuth Client	agenta:
      * Using Dynamic Server: disabled
      * Server: z kroku 2.3
      * Grant Type: Authorization Code
      * OpenID Connect: Enabled
      * OpenID Connect Flow Type: Authorization Code
      * Authentication Redirect Request: /Common/F5AuthRedirectRequestJWT
      * Token Request: /Common/F5TokenJWTRequestByAuthzCode
      * Refresh Token Request: /Common/F5TokenRefreshRequest
      * OpenID Connect UserInfo Request: /Common/F5UserInfoRequest
      * Redirection URI - nechat jak je - `https://%{session.server.network.name}/oauth/client/redirect`
      * Scope: podle 1.3
      * Save
    * Apply
  * Priradit k VS






